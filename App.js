
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import TelaInicial from './src/telaInicial';
import SegundaTela from './src/segundaTela';
import Menu from './src/components/menu';
import Components from './src/components/componentes';
import Layout from './src/layout/layoutTela';
import Saudacoes from './src/exerc/saudacoes'
import Opacity from './src/exerc/opacity'
import Contagem from './src/exerc/contagem'

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
      <Stack.Screen name="Menu" component={Menu}/>
        <Stack.Screen name="TelaInicial" component={TelaInicial}/>
        <Stack.Screen name="SegundaTela" component={SegundaTela}/>
        <Stack.Screen name="Componentes" component={Components}/>
        <Stack.Screen name="LayoutTela" component={Layout}/>
        <Stack.Screen name="Saudações" component={Saudacoes}/>
        <Stack.Screen name="Opacidade" component={Opacity}/>
        <Stack.Screen name="Contador" component={Contagem}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}


  

