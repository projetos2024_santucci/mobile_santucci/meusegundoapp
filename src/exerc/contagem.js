import React, { useState } from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet, ScrollView } from 'react-native';

const App = () => {
  const [contador, setContador] = useState(0);
  const [dado, setDado] = useState(0);
  function rodarDado(){
    setDado(Math.floor(Math.random() * 6) + 1);
  }
  const [backgroundImage, setBackgroundImage] = useState('https://pbs.twimg.com/media/FNM8VJqXwAMIG2C.png');
  const [catImage, setCatImage] = useState('https://pbs.twimg.com/media/FNM8VJqXwAMIG2C.png');

  const handleBackgroundPress = () => {
    setContador(contador + 1);
    if (contador + 1 === 10) {
      setBackgroundImage('https://m.media-amazon.com/images/I/61xt9huljYL._AC_UF894,1000_QL80_.jpg');
      setCatImage('https://i.pinimg.com/originals/85/4f/8d/854f8d7552f3d3c752338e4b12ab6ffa.jpg');
    }
  };

  const handleCatPress = () => {
    setContador(contador - 1);
    if (contador - 1 === -1) {
      setBackgroundImage('https://img.quizur.com/f/img63b3485092d035.03451113.jpg?lastEdited=1672693867');
      setCatImage('https://pbs.twimg.com/media/FvY6Gp9WwAM5e1z.jpg');
    }
  };
  

  return (
    
    <View style={styles.container}>
      <TouchableOpacity onPress={handleBackgroundPress}>
        <Image source={{ uri: backgroundImage }} style={styles.backgroundImage} />
      </TouchableOpacity>
      <TouchableOpacity onPress={handleCatPress}>
        <Image source={{ uri: catImage }} style={styles.catImage} />
      </TouchableOpacity>
      <Text style={styles.counterText}>O NÍVEL DE BELEZA DOS INDIVÍDUOS SE ENCONTRA EM: {contador}</Text>
      <TouchableOpacity onPress={() => rodarDado()}>
        <Text>Jogue o dado</Text>
      </TouchableOpacity>
      <Text>Número sorteado: {dado}</Text>
    </View>
   
    
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backgroundImage: {
    width: 200,
    height: 200,
    marginBottom: 10,
  },
  catImage: {
    width: 150,
    height: 150,
    marginBottom: 10,
  },
  counterText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
});


export default App;
