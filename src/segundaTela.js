import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Button } from 'react-native';


function SegundaTela({navigation}) {
    return(
    <View style={styles.container}>
      <Text>Segunda Tela</Text>
      <Button title="Voltar para home" onPress={()=> navigation.goBack()}></Button>
    </View>
  )
}

export default SegundaTela;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: 'center'
  }
});
