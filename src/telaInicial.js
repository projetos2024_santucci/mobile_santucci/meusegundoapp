import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Button } from 'react-native';

function TelaInicial({navigation}) {
  return(
    <View style={styles.container}>
      <Text>Tela Inicial</Text>
      <Button title="Ir para segunda página" onPress={()=> navigation.navigate('SegundaTela')}></Button>
    </View>
  )
}

export default TelaInicial;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: 'center'
  }
  

});
