import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';
import Layout from '../layout/layoutTela';
import LayoutGrade from '../layout/layoutGrade';
import Components from './componentes';
import Saudacoes from '../exerc/saudacoes'
import Opacity from '../exerc/opacity'
import Contagem from '../exerc/contagem'


export default function Menu({navigation}) {
  return (
    <View style={styles.container}>
     <TouchableOpacity style={styles.menu} onPress={()=> navigation.navigate('Componentes')}>
    <Text>Opção 1</Text>
     </TouchableOpacity>
     <TouchableOpacity style={styles.menu} onPress={()=> navigation.navigate('LayoutTela')}>  
     <Text>Opção 2</Text>
     </TouchableOpacity>
     <TouchableOpacity style={styles.menu} onPress={()=> navigation.navigate('Saudações')} >
     <Text>Opção 3</Text>
     </TouchableOpacity>
     <TouchableOpacity style={styles.menu}  onPress={()=> navigation.navigate('Opacidade')}>
     <Text>Opção 4</Text>
     </TouchableOpacity>
     <TouchableOpacity style={styles.menu}  onPress={()=> navigation.navigate('Contador')}>
     <Text>Opção 5</Text>
     </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: 'center'
  },
  menu:{
    padding: 10,
    margin: 5,
    backgroundColor: 'lightblue',
    borderRadius: 5,
  }

});
