import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, ScrollView } from "react-native";
import Layout from "../layoutTela";

export default function LayoutHorizontal() {
  return (
    <View style={styles.container}>
        <View>
        <ScrollView horizontal={true}>

      <View style={styles.box1}></View>
      <View style={styles.box2}></View>
      <View style={styles.box3}></View>
      <View style={styles.box1}></View>
      <View style={styles.box2}></View>
      <View style={styles.box3}></View>
      <View style={styles.box1}></View>
      <View style={styles.box2}></View>
     
      </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
  },

  box1: {
    width: 50,
    height: 50,
    backgroundColor: "red",
    borderRadius: 150
  },
  box2: {
    width: 50,
    height: 50,
    backgroundColor: "green",
    borderRadius: 150
  },
  box3: {
    width: 50,
    height: 50,
    backgroundColor: "blue",
    borderRadius: 150
  },
});
