import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView } from 'react-native';

export default function Layout() {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
    <Text style={styles.text}>Menu</Text>
      </View>

      <View style={styles.content}>
      <ScrollView>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>
      <Text>Conteúdo</Text>

      </ScrollView>
      </View>

      <View style={styles.footer}>
      <Text>Rodapé</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
  },

  header:{
    height: 50,
    backgroundColor: 'blue',

    
  },

  content:{
    flex: 1,
    backgroundColor: 'green'
  },

  footer:{
    height: 50,
    backgroundColor: 'red',
    alignItems: 'center',
    fontSize: 15
  },

  text:{
    fontSize: 30,
    textAlign: 'center'
  },
});
